package pack.studqr.employeeapiexample.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {
    @POST("create")
    Call<EmployeeResponse> addEmployee(@Body Employee e);

    @GET("employees")
    Call<EmployeesShowResponse> getAllEmployee();

    @DELETE("delete/{id}")
    Call<EmployeesShowResponse> deleteEmployee(@Path("id") int id);
}
