package pack.studqr.employeeapiexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import pack.studqr.employeeapiexample.api.EmployeeData;

public class EmployeeAdapter  extends RecyclerView.Adapter<EmployeeAdapter.EmployeeHolder>{

    private Context context;
    private List<EmployeeData> employeeData;

    public EmployeeAdapter(Context context, List<EmployeeData> employeeData) {
        this.context = context;
        this.employeeData = employeeData;
    }

    @NonNull
    @Override
    public EmployeeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflater = LayoutInflater.from(context).inflate(R.layout.rv_cell_employee_details,parent,false);
        return new EmployeeHolder(inflater);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeHolder holder, int position) {
       holder.age.setText(""+employeeData.get(position).getEmployeeAge());
        holder.name.setText(employeeData.get(position).getEmployeeName());
        holder.id.setText(""+employeeData.get(position).getId());
    }


    @Override
    public int getItemCount() {
        return employeeData.size();
    }

    class EmployeeHolder extends RecyclerView.ViewHolder{
        TextView id,name,age;
        public EmployeeHolder(@NonNull View itemView) {
            super(itemView);
            age = itemView.findViewById(R.id.employeeAge);
            name = itemView.findViewById(R.id.employeeName);
            id = itemView.findViewById(R.id.employeeId);
        }
    }
}
