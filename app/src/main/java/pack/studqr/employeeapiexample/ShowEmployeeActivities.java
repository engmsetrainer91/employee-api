package pack.studqr.employeeapiexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import pack.studqr.employeeapiexample.api.ApiClient;
import pack.studqr.employeeapiexample.api.ApiInterface;
import pack.studqr.employeeapiexample.api.EmployeesShowResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.widget.Toast;

public class ShowEmployeeActivities extends AppCompatActivity {

    RecyclerView recyclerView;
    EmployeeAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_list);
        recyclerView = findViewById(R.id.employeeList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        fetchEmployee();
    }

    private  void fetchEmployee(){
        ApiInterface apiInterface = ApiClient.getRetrofitClient().create(ApiInterface.class);
        Call<EmployeesShowResponse> employeesShowResponseCall = apiInterface.getAllEmployee();
        employeesShowResponseCall.enqueue(new Callback<EmployeesShowResponse>() {
            @Override
            public void onResponse(Call<EmployeesShowResponse> call, Response<EmployeesShowResponse> response) {

                adapter = new EmployeeAdapter(getApplicationContext(),  response.body().getData());
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<EmployeesShowResponse> call, Throwable t) {
                Toast.makeText(ShowEmployeeActivities.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}