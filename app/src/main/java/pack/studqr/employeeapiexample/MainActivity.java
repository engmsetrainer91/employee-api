package pack.studqr.employeeapiexample;

import androidx.appcompat.app.AppCompatActivity;
import pack.studqr.employeeapiexample.api.ApiClient;
import pack.studqr.employeeapiexample.api.ApiInterface;
import pack.studqr.employeeapiexample.api.Employee;
import pack.studqr.employeeapiexample.api.EmployeeResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText name,age,salary;
    Button add,show;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = findViewById(R.id.nameEt);
        age = findViewById(R.id.ageEt);
        salary = findViewById(R.id.salaryEt);
        add = findViewById(R.id.addBtn);
        show = findViewById(R.id.showBtn);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Employee e1 = new Employee();
               e1.setName(name.getText().toString());
               e1.setAge(age.getText().toString());
               e1.setSalary(salary.getText().toString());
               addEmployee(e1);

            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             startActivity(new Intent(MainActivity.this,ShowEmployeeActivities.class));
            }
        });
    }

    private void addEmployee(Employee e){
        ApiInterface apiInterface = ApiClient.getRetrofitClient().create(ApiInterface.class);
        Call<EmployeeResponse> responseCall= apiInterface.addEmployee(e);
        responseCall.enqueue(new Callback<EmployeeResponse>() {
            @Override
            public void onResponse(Call<EmployeeResponse> call, Response<EmployeeResponse> response) {
               String msg= response.body().getMessage();
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<EmployeeResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}